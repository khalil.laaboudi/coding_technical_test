package com.codingChallenge.kotlinspringbootmysql.controller


import com.codingChallenge.kotlinspringbootmysql.model.Product
import com.codingChallenge.kotlinspringbootmysql.repository.ProductRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = arrayOf("http://localhost:3000"))
class ProductController(private val productRepository: ProductRepository) {

    @GetMapping("/products")
    fun getAllProducts(): List<Product> =
            productRepository.findAll()


    @PostMapping("/products")
    fun createNewProduct(@Valid @RequestBody product: Product): Product {
        return  productRepository.save(product);
    }



    @GetMapping("/products/{id}")
    fun getProductById(@PathVariable(value = "id") productId: Long): ResponseEntity<Product> {
        return productRepository.findById(productId).map { product ->
            ResponseEntity.ok(product)
        }.orElse(ResponseEntity.notFound().build())
    }

    @PutMapping("/products/{id}")
    fun updateProductById(@PathVariable(value = "id") productId: Long,
                          @Valid @RequestBody newProduct: Product): ResponseEntity<Product> {

        return productRepository.findById(productId).map { existingProduct ->
            val updatedProduct: Product = existingProduct
                    .copy(title = newProduct.title, price = newProduct.price)
            ResponseEntity.ok().body(productRepository.save(updatedProduct))
        }.orElse(ResponseEntity.notFound().build())

    }

    @DeleteMapping("/products/{id}")
    fun deleteProductById(@PathVariable(value = "id") productId: Long): ResponseEntity<Void> {

        return productRepository.findById(productId).map { product  ->
            productRepository.delete(product)
            ResponseEntity<Void>(HttpStatus.OK)
        }.orElse(ResponseEntity.notFound().build())

    }
}


