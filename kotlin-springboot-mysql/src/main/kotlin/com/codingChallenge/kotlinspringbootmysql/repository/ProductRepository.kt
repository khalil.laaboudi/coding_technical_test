package com.codingChallenge.kotlinspringbootmysql.repository

import com.codingChallenge.kotlinspringbootmysql.model.Product
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : JpaRepository<Product, Long>