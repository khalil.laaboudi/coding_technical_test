package com.codingChallenge.kotlinspringbootmysql.repository

import com.codingChallenge.kotlinspringbootmysql.model.Category
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CategoryRepository : JpaRepository<Category, Long>{
    fun findByParentCategoryNull() : List<Category>;
}