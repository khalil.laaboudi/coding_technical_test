package com.codingChallenge.kotlinspringbootmysql

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinSpringbootMysqlApplication

fun main(args: Array<String>) {
	runApplication<KotlinSpringbootMysqlApplication>(*args)
}
