DROP SCHEMA IF EXISTS `codding-challeng`;

CREATE SCHEMA `coding-challenge-database`;

use `coding-challenge-database`;

SET FOREIGN_KEY_CHECKS = 0;



DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `first_name` varchar(45) DEFAULT NULL,
                              `last_name` varchar(45) DEFAULT NULL,
                              `email` varchar(45) DEFAULT NULL,
                              `password` varchar(100) DEFAULT NULL,

                              PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `day`;

CREATE TABLE `day` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `date` DATE NOT NULL ,
                          `user_id` int(11) ,

                          PRIMARY KEY (`id`),

                          FOREIGN KEY (user_id)
                              REFERENCES user(id)

                                  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;





DROP TABLE IF EXISTS `objective`;

CREATE TABLE `objective` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `name` varchar (45)  DEFAULT NULL ,
                        `finished` TINYINT DEFAULT 1,
                        `day_id` int(11) ,

                       PRIMARY KEY (`id`),

                       FOREIGN KEY (day_id)
                           REFERENCES day(id)

                           ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;






DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `name` varchar (45)  DEFAULT NULL ,
                             `description` varchar (45)  DEFAULT NULL ,
                             `tag` varchar (45)  DEFAULT NULL ,
                             `start_time` DATE NOT NULL ,
                             `finish_time` DATE NOT NULL ,
                             `day_id` int(11) ,

                             PRIMARY KEY (`id`),

                             FOREIGN KEY (day_id)
                                 REFERENCES day(id)

                                 ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;






INSERT INTO user (first_name, last_name, email,password)
VALUES ("laaboudi", "khalil", "khalil.laaboudi@uit.ac.ma", "123456");

INSERT INTO user (first_name, last_name, email,password)
VALUES ("laaboudi", "zinelaabidine", "zine.laaboudi@uit.ac.ma", "000000");


SET FOREIGN_KEY_CHECKS = 1;
